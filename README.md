# 2180607518
|       Title    		|Customer check out cart and makes payment            
|------------------------|-------------------------------|
| Value | As a customer, I want to be check out my cart and payment, so I can review the products I choose.|
|Acceptance Criteria	|1. Adding Items to Cart: Users can add products to the cart by clicking the "Add to Cart" button on product pages.<br> 2. Cart Interaction:  Users can easily change the quantity of items in the cart (increase/decrease/remove).<br>3. Shipping and Tax Calculation: The cart calculates shipping fees based on the user's selected shipping method and location.<br>4. Payment Information: Users can enter payment information securely, including credit card details or other payment methods.<br>5. Order Review: Users can review their delivery information and upon receipt can review the item.<br>6. Payment Processing: After confirming the order, the system processes the payment securely and efficiently.<br>7. Payment Gateway Integration: Can link with banks and other payment platforms, Payment gateway integration is thoroughly tested to ensure successful transaction processing.             |
|Definition of Done         |1.Functional Requirements.<br>2.User Acceptance Testing.<br>3.User Interface (UI/UX).<br>4.Data Management <br>5.Testing and Verification. |
|Owner          |Responsible person: Huynh Truong Hoan|
|Interation | Unscheduled|
|Estimate: | 5 Points |

![hoan1](/uploads/b38d43679bdc772d530c8933d8c85655/hoan1.png)
![hoan2](/uploads/f4ebc2b209b3dd7a1a778fb678f5c746/hoan2.png)



# 2180608413
|       Title    		| Search books |
|------------------------|-------------------------------|
|Value Statement	|As a customer, I want to find books quickly so I can view or buy the books I want            |
|Acceptance Criteria        |Given that  to already know the information of book .<br>When customers enter into the search box.<br> Then a list of related books appears for customers to search.|
| Definition of Done| 1. performs validation and error handling for inputs and outputs.<br>2.meets the performance, security, usability, and reliability requirements.<br>3. passes all the unit tests, integration tests, and acceptance tests.
|Owner          |Hoai Phuong| |
 | Interation: | Unscheduled 
 | Estimate: | 5 point

![phuong](/uploads/f3b2182199941db61bfaf934b221b2f8/phuong.png)


Lâm Nhật Khang

| Title: | User Feedback Feature |
| :-------- | :----------------- |
| Value Statement: | As a customer, <br>I would like to be able to give feedback on the product, <br> my experience or submit store advice, to share your opinion and help improve the store's services.  |
| Acceptance Criteria: | Scenario 1:  <br>Given I am on the home page or in a dedicated section for feedback. <br>When I click on a "Feedback" or "Provide Feedback" button or link. <br>Then I am filling out the feedback form. <br> <br> <br>Scenario 2: <br>Given I have submitted feedback through the application.<br>When I want to check the status of my feedback.<br>Then there should be a "Feedback History" or "My Feedback" section in the application where I can view the status and responses to my feedback. <br> |
| Definition of Done: |  - Unit test passed <br> - Acceptance criteria are met <br> - The code has been evaluated <br> - Functional test passed <br> - Non-functional requirements have been met <br> - Save and rate tests |
| Owner: | Khang | Owner |
| Iteration: | Unscheduled |
| Estimate: | 5 Points |






![khang](/uploads/9d50a175da06c3f63ac4f85c318a0673/khang.png)


# 2180608203


|      Title          |   Show menu of books                                         ||
|----------------|-------------------------------|-----------------------------|
|Value Statement|        As a bookseller, I want to be able to view a menu that lists all the available books to the customer, so that they can easily browse and select the book they want to buy. |          |
|Acceptance Criteria          |Scenario 1 : <br>Given the menu display a list of books, each book should be displayed with its title, author, and a brief description,When the customers open app or click on the list of books, Then a list with many book genres appears, this menu should be easy to navigate and scroll through and Clicking on a book should open a detailed view or page providing more information and option as adding it to wishlist or purchasing it              |      |     
|Definition of Done         |Unit Tests Passed <br> Acceptance Criteria Met <br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story||
|		Owner			|			Tran Thanh Tuong					|			|				
|			Iteration		|		Unscheduled							|			|			
|					Estimate|5 Points								||

![giaodienus](/uploads/53cbd18a0a2a51991ee2ee82512dc6a1/giaodienus.png)
![giaodientc](/uploads/97c7431170781f0f9c3a64ca2776185d/giaodientc.png)

#2180608573

| Title: | Manage Books |
| ------ | ------ |
| Value Statement: | As a book manager, I want to update, delete and search books in the database, so that I can manage the inventory and availability of books. |
| Acceptance Criteria: | <ul> <li>The book manager can access a web page with a form to enter the book details (title, author, genre, ISBN, etc.)</li> <li>The book manager can submit the form and see a confirmation message that the book has been added to the database</li> <li>The book manager can view a list of all the books in the database with their details</li> <li>The book manager can edit or delete any book from the list by clicking on a button</li> <li>The book manager can search for a book by entering a keyword or a filter (e.g. genre, author, etc.)</li> </ul> |
| Definition of Done | <ul> <li>Acceptance Criteria Met</li> <li>Code reviewed</li> <li>Product owner Accepts User story</li> </ul> |
| Owner | Book Manager |
| Iteration | Unscheduled |
| Estimate | 5 Points |






# Main User Interface
![mainUS](/uploads/f07662db0d782d0d1ed4f71cde4fa9e8/mainUS.png)


# Menu Items – Specific Books
![menuUS](/uploads/8b9c037f7e27ea99c70bb6df5c7094ef/menuUS.png)

![comboBoxUS](/uploads/975a4b59a755f1ce25eca80318f054e5/comboBoxUS.png)

# Form Edit Book
![editUS](/uploads/60e1fcbc84ab17e931b5b3385d91499c/editUS.png)

# Form Book Detail
![detailUS](/uploads/fb4a7c2b2dce0c1c78349b6245831b2b/detailUS.png)

# Form Import Books
![importUS](/uploads/ac0485730f7df9606ae8ee45bd48fa1a/importUS.png)


